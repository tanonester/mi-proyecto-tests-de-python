from math import pi

def areaCirculo(radio):
	if type(radio) not in [int, float]:
		raise TypeError("El radio debe ser un entero no negativo")
		

	if radio < 0:
		raise ValueError("El radio no puede ser negativo.")
	return pi*(radio**2)

"""
#Funcion de testing
radii = [2, 0, -3, 2 + 5j, True, "radio"]
mensaje = "El area de los circulos con radio = {Radio} es {area}."

for r in radii:
	A = areaCirculo(r)
	print(mensaje.format(Radio=r, area=A))

"""