import unittest
from circulos import areaCirculo
from math import pi

class TestCirculoArea(unittest.TestCase):
	def test_area(self):
		# Test area cuando  el radio es >= 0
		self.assertAlmostEqual(areaCirculo(1), pi),
		self.assertAlmostEqual(areaCirculo(0), 0),
		self.assertAlmostEqual(areaCirculo(2.1), pi * 2.1**2)


	def test_valores(self):
		#Asegurase que los value errors se ejecuten cuando sea necesario
		self.assertRaises(ValueError, areaCirculo, -2)		

	def test_tipos(self):
		#Asegurarse que los value errors se ejecuten cuando sea necesario
		self.assertRaises(TypeError, areaCirculo, 3+5j)
		self.assertRaises(TypeError, areaCirculo, True)
		self.assertRaises(TypeError, areaCirculo, "radio")
		