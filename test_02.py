def compruebaMail(mailUsuario):

	"""
	Metodo que comprueba si el amil tiene un solo @ y no estar al final para devolver True
	>>> compruebaMail('juan@gnail.com')
	True
	>>> compruebaMail('uno.com@')
	False
	>>> compruebaMail('uno@@hmail.com')
	False
	>>> compruebaMail('uno.com')
	False
	>>> compruebaMail('uno.com@')
	True
	>>> compruebaMail('uno.com@')
	False

	"""

	arroba=mailUsuario.count('@')

	if(arroba!=1 or mailUsuario.rfind("@")==(len(mailUsuario)-1) or mailUsuario.find('@')==0):
		return False

	else:
		return True

import doctest
doctest.testmod()
