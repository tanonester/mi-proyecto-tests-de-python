import math

def raizCuadrada(ListaNumeros):

	"""
	Metodo devuelve  Double otra lista de la raiz cuadrada de los elementos ingresados

	>>> lista=[]
	>>> for i in [4, 9, 16]:
	...		lista.append(i)
	>>> raizCuadrada(lista)
	[2.0, 3.0, 4.0]

	>>> lista=[]
	>>> for i in [4, -9, 16]:
	...		lista.append(i)
	>>> raizCuadrada(lista)
	Traceback (most recent call last):
		...
	ValueError: math domain error



	"""

	return [math.sqrt(n) for n in ListaNumeros]	


#print (raizCuadrada([16, -4, 9, 25]))

import doctest
doctest.testmod()